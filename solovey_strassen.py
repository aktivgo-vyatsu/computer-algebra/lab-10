import math
import random


def solovey_strassen_is_composite(n, iterations) -> (bool, float):
    if n <= 3 or not n % 2:
        raise ValueError('n should be an odd positive integer greater then 3')
    for _ in range(iterations):
        a = random.randrange(2, n - 2)
        if math.gcd(a, n) != 1:
            return True, 0
        t = pow(a, int((n - 1) / 2)) % n
        if abs(t - jacobi_symbol(a, n)) % n != 0:
            return True, 0
    return False, (1 - (1 / (2 ** iterations))) * 100


def jacobi_symbol(m: int, n: int) -> int:
    if n < 0 or not n % 2:
        raise ValueError('n should be an odd positive integer')
    if m < 0 or m > n:
        m %= n
    if not m:
        return int(n == 1)
    if n == 1 or m == 1:
        return 1
    if math.gcd(m, n) != 1:
        return 0

    j = 1
    while m != 0:
        while m % 2 == 0 and m > 0:
            m >>= 1
            if n % 8 in [3, 5]:
                j = -j
        m, n = n, m
        if m % 4 == n % 4 == 3:
            j = -j
        m %= n
    return j
