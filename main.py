from fermat import *
from solovey_strassen import *
from miller_rabin import *

if __name__ == '__main__':
    n = 9973

    is_composite, percent = fermat_is_composite(n, 5)
    print(
        f'Тест Ферма - число {n}:',
        'составное' if is_composite else f'возможно простое с вероятностью {percent}',
    )

    is_composite, percent = solovey_strassen_is_composite(n, 5)
    print(
        f'Тест Соловея-Штрассена - число {n}:',
        'составное' if is_composite else f'возможно простое с вероятностью {percent}',
    )

    is_composite, percent = miller_rabin_is_composite(n, 5)
    print(
        f'Тест Миллера-Рабина - число {n}:',
        'составное' if is_composite else f'возможно простое с вероятностью {percent}',
    )
