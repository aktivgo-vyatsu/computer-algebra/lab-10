import math
import random


def fermat_is_composite(n: int, iterations: int) -> (bool, float):
    if n <= 3 or not n % 2:
        raise ValueError('n should be an odd positive integer greater then 3')
    for _ in range(iterations):
        a = random.randrange(2, n - 2)
        if math.gcd(a, n) != 1:
            return True, 0
        t = pow(a, n - 1, n)
        if t != 1:
            return True, 0
    return False, (1 - (1 / (2 ** iterations))) * 100
