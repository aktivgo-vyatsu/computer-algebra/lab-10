import random


def miller_rabin_is_composite(n: int, iterations: int) -> (bool, float):
    if n < 5 or not n % 2:
        raise ValueError('n should be an odd positive integer greater then 3')

    s = 0
    t = n - 1
    while t % 2 == 0:
        t >>= 1
        s += 1

    if 2 ** s * t != n - 1:
        raise ValueError('s calculation error')

    def trial_composite(a: int) -> bool:
        if pow(a, t, n) == 1:
            return False
        for i in range(s):
            if pow(a, 2 ** i * t, n) == n - 1:
                return False
        return True

    for i in range(iterations):
        a = random.randrange(2, n)
        if trial_composite(a):
            return True, 0

    return False, (1 - (1 / (4 ** iterations))) * 100
